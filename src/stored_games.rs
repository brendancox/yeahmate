use chess_engine::ChessPiece;
use rusqlite::{Connection, Transaction, params};
use std::{collections::HashMap, fs::File};
use std::io::{BufReader, BufRead};
use super::chess_engine as chess_engine;
use std::time::SystemTime;

pub fn create_tables(conn: &Connection) {
    let statement = "CREATE TABLE IF NOT EXISTS stored_game_sets (
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL
    )";

    conn.execute(statement, params![]).unwrap();

    let statement = "CREATE TABLE IF NOT EXISTS stored_games (
        id INTEGER PRIMARY KEY,
        set_id INTEGER,
        player_white TEXT NOT NULL,
        player_black TEXT NOT NULL,
        player_is INTEGER,
        result INTEGER,
        site TEXT,
        white_rating INTEGER,
        black_rating INTEGER,
        time_control TEXT,
        date TEXT,
        quickstore INTEGER,
        pgnmoves TEXT
    )";

    conn.execute(statement, params![]).unwrap();

    let statement = "CREATE TABLE IF NOT EXISTS stored_moves (
        id INTEGER PRIMARY KEY,
        game_id INTEGER,
        halfmove INTEGER,
        from_rank INTEGER,
        from_file INTEGER,
        from_piece INTEGER,
        to_rank INTEGER,
        to_file INTEGER,
        to_piece INTEGER,
        promote INTEGER,
        en_passant INTEGER,
        castle INTEGER,
        pre_no_move_fen TEXT,
        resulting_no_move_fen TEXT,
        colour INTEGER,
        in_check INTEGER,
        checkmate INTEGER,
        move_san TEXT
    )";

    conn.execute(statement, params![]).unwrap();

    let statement = "CREATE TABLE IF NOT EXISTS quickstore_moves (
        id INTEGER PRIMARY KEY,
        game_id INTEGER,
        halfmove INTEGER,
        move_san TEXT
    )";

    conn.execute(statement, params![]).unwrap();

    let statement = "CREATE INDEX IF NOT EXISTS quichalf ON quickstore_moves(halfmove);";
    conn.execute(statement, params![]).unwrap();
}

#[derive(PartialEq)]
enum GameReadStage {
    Headers,
    MoveText,
    Store
}

pub fn store_games_from_file(path: String, set_name: String, player_name: &String, quickstore: bool) {

    let now = SystemTime::now();

    let mut conn = Connection::open("gdchess.db").unwrap();

    let set_id = create_game_set(&conn, set_name);

    match File::open(path) {
        Ok(file) => {
            let mut headers = vec![];
            let mut movetext = "".to_owned();
            let mut game_read_stage = GameReadStage::Headers;
            let mut legal_moves_cache = HashMap::new();
            let mut games_to_store = vec![];
            let mut total_games_imported = 0;
            for line in BufReader::new(file).lines() {
                match line {
                    Ok(line) => {
                        let line = line.trim();
                        match game_read_stage {
                            GameReadStage::Headers => {
                                if line == "" {
                                    if !headers.is_empty() {
                                        game_read_stage = GameReadStage::MoveText;
                                    }
                                } else {
                                    headers.push(line.to_owned());
                                }
                            },
                            GameReadStage::MoveText => {
                                if line == "" {
                                    if movetext != "".to_owned() {
                                        game_read_stage = GameReadStage::Store;
                                    }
                                } else {
                                    movetext += " ";
                                    movetext += line;
                                }
                            },
                            _ => ()
                        }
                        
                    },
                    _ => ()
                }

                if game_read_stage == GameReadStage::Store {
                    games_to_store.push((headers, movetext));
                    game_read_stage = GameReadStage::Headers;
                    headers = vec![];
                    movetext = "".to_owned();

                    if games_to_store.len() > 500 {
                        total_games_imported += 500;
                        let mut tx = conn.transaction().unwrap();
                        for (headers_i, movetext_i) in games_to_store.iter() {
                            store_a_game(&mut tx, set_id, &headers_i, &movetext_i, player_name, &mut legal_moves_cache, quickstore);
                        }
                        tx.commit().unwrap();
                        let time_taken =  now.elapsed().unwrap().as_secs();
                        // Indicate progress is happening.
                        println!("{} mins: have imported {} games. moves cach size: {}", time_taken as f64 / 60.0, total_games_imported, legal_moves_cache.len());
                        games_to_store.clear();
                    }
                }
            }

            if games_to_store.len() > 0 {

                total_games_imported += games_to_store.len();
                let mut tx = conn.transaction().unwrap();
                for (headers_i, movetext_i) in games_to_store.iter() {
                    store_a_game(&mut tx, set_id, &headers_i, &movetext_i, player_name, &mut legal_moves_cache, quickstore);
                }
                tx.commit().unwrap();
                let time_taken =  now.elapsed().unwrap().as_secs();
                // Indicate progress is happening.
                println!("{} mins: have imported {} games. moves cach size: {}", time_taken as f64 / 60.0, total_games_imported, legal_moves_cache.len());
                games_to_store.clear();
            }

            let time_taken =  now.elapsed().unwrap().as_secs();
            if time_taken < 60 {
                println!("Imported set in {} seconds", time_taken);
            } else {
                println!("Imported set in {} minutes", time_taken as f64 / 60.0);
            }
            println!("Cache size {}", legal_moves_cache.len());
        },
        _ => ()
    }
}

fn create_game_set(conn: &Connection, name: String) -> i64 {
    conn.execute("INSERT INTO stored_game_sets (name) VALUES (?)", vec![name]).unwrap();
    conn.last_insert_rowid()
}

pub enum GameResult {
    NoResult,
    Draw,
    WhiteWins,
    BlackWins
}

#[derive(Copy, Clone)]
pub enum PlayerIs {
    Neither,
    White,
    Black
}

impl GameResult {
    fn from_text(text: &str) -> GameResult {
        match text {
            "*" => GameResult::NoResult,
            "1/2-1/2" => GameResult::Draw,
            "1-0" => GameResult::WhiteWins,
            "0-1" => GameResult::BlackWins,
            invalid => panic!("Invalid result text: {}", invalid)
        }
    }

    pub fn from_i64(number: i64) -> GameResult {
        let no_result = GameResult::NoResult as i64;
        let draw = GameResult::Draw as i64;
        let white = GameResult::WhiteWins as i64;
        let black = GameResult::BlackWins as i64;
        match number {
            no_result => GameResult::NoResult,
            draw => GameResult::Draw,
            white => GameResult::WhiteWins,
            black => GameResult::BlackWins,
            _ => panic!("no gameresult constant found for {}", number)
        }
    }
}

fn store_a_game(tx: &mut Transaction, set_id: i64, headers: &Vec<String>, movetext: &String, player_name: &String,
     legal_moves_cache: &mut HashMap<String, Vec<chess_engine::ChessMove>>, quickstore: bool) {

    let mut player_white = "".to_owned();
    let mut player_black = "".to_owned();
    let mut player_is = PlayerIs::Neither;
    let mut result = GameResult::NoResult as i64;
    let mut site = "".to_owned();
    let mut white_rating = 0;
    let mut black_rating = 0;
    let mut time_control = "".to_owned();
    let mut date = "".to_owned();

    for header in headers {
        let (key, value) = parse_pgn_header(header);
        match key.as_str() {
            "White" => {
                if player_name != "" && player_name == &value {
                    player_is = PlayerIs::White;
                }
                player_white = value;
            },
            "Black" => {
                if player_name != "" && player_name == &value {
                    player_is = PlayerIs::Black;
                }
                player_black = value;
            },
            "Result" => result = GameResult::from_text(&value) as i64,
            "Site" => site = value,
            "WhiteElo" => white_rating = {
                match value.parse() {
                    Ok(rating) => rating,
                    Err(_) => 0
                }
            },
            "BlackElo" => black_rating = {
                match value.parse() {
                    Ok(rating) => rating,
                    Err(_) => 0
                }
            },
            "TimeControl" => time_control = value,
            "Date" => date = value,
            "Variant" => {
                if value.as_str() != "Standard" {
                    return;
                }
            }
            _ => ()
        }
    }

    let mut header_statement = tx.prepare_cached("INSERT INTO stored_games (set_id, player_white, player_black,
        player_is, result, site, white_rating, black_rating, time_control, date, quickstore, pgnmoves)
         VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)").unwrap();

    header_statement.execute(params![set_id, player_white, player_black, player_is as i64, result, site, white_rating,
         black_rating, time_control, date, quickstore, movetext.trim()]).unwrap();
    
    let game_id = tx.last_insert_rowid();
    
    let result_sans = vec!["1-0", "0-1", "1/2-1/2"];

    if quickstore {

        let mut halfmove = 0;

        for sanmove in movetext.split_whitespace() {
            if sanmove.contains(|c| c == '.') {
                // Remove move numbers.
                continue;
            }
            if result_sans.contains(&sanmove) {
                break;
            }
            
            let mut quickmove_statement = tx.prepare_cached("INSERT INTO quickstore_moves (game_id, halfmove, move_san) VALUES (?, ?, ?)").unwrap();
            quickmove_statement.execute(params![game_id, halfmove as i64, sanmove]).unwrap();
            
            halfmove += 1;
        }

    } else {

        let mut game = chess_engine::GameInstance::starting_position();
        let mut halfmove = 0;

        for sanmove in movetext.split_whitespace() {
            if sanmove.contains(|c| c == '.') {
                // Remove move numbers.
                continue;
            }
            if result_sans.contains(&sanmove) {
                break;
            }

            match game.create_move_from_san(sanmove, legal_moves_cache, halfmove) {
                Ok(chessmove) => game.do_move(&chessmove, true, Some(legal_moves_cache)),
                Err(error_string) => {
                    // Print lots of debug info.
                    println!("Import failed. Previous moves:");
                    for played_move in game.played_moves.iter() {
                        println!("For move {}: {}", played_move.move_san, played_move.resulting_no_moves_fen);
                    }

                    println!("Legal moves available:");
                    let files = vec!['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
                    let ranks = vec!['8', '7', '6', '5', '4', '3', '2', '1'];

                    let legal_moves = game.legal_moves_for_colour(game.side_to_move);
                    for chessmove in legal_moves {
                        let mvepiece = game.placements[chessmove.from.0][chessmove.from.1];
                        println!("{} to {}{}", mvepiece.character(), files[chessmove.to.1], ranks[chessmove.to.0]);
                    }

                    panic!(error_string);
                }
            }

            halfmove += 1;
        }

        for (halfmove, played_move) in game.played_moves.iter().enumerate() {

            let promote = match played_move.promote {
                Some(ppiece) => ppiece,
                None => ChessPiece::NoPiece
            };

            let mut moves_statement = tx.prepare_cached("INSERT INTO stored_moves (
                game_id,
                halfmove,
                from_rank,
                from_file,
                from_piece,
                to_rank,
                to_file,
                to_piece,
                promote,
                en_passant,
                castle,
                pre_no_move_fen,
                resulting_no_move_fen,
                colour,
                in_check,
                checkmate,
                move_san)
                 VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                ).unwrap();

            moves_statement.execute(params![
                    game_id,
                    halfmove as i64,
                    played_move.from.0 as i64,
                    played_move.from.1 as i64,
                    played_move.from.2 as i64,
                    played_move.to.0 as i64,
                    played_move.to.1 as i64,
                    played_move.to.2 as i64,
                    promote as i64,
                    played_move.en_passant as i64,
                    played_move.castle as i64,
                    played_move.pre_no_move_fen,
                    played_move.resulting_no_moves_fen,
                    played_move.colour as i64,
                    played_move.check as i64,
                    played_move.checkmate as i64,
                    played_move.move_san
                 ]).unwrap();
        }

    }
}

fn parse_pgn_header(header: &String) -> (String, String) {
    let trimmed = header.trim_matches(|c| c == '[' || c == ']');
    let mut parts: Vec<&str> = trimmed.split_whitespace().collect();
    let value: Vec<&str> = parts.drain(1..).collect();
    let value = value.join( " ");
    let value = value.trim_matches(|c| c == '"' || c == '"');
    (parts[0].to_owned(), value.to_owned())
}


#[cfg(test)]
mod tests {

    use super::*;
    use std::time::{SystemTime};

    fn setup() {
        let conn = Connection::open("gdchess.db").unwrap();
        create_tables(&conn);
    }

    fn teardown() {
        let conn = Connection::open("gdchess.db").unwrap();
        conn.execute(
            "DROP TABLE stored_game_sets; DROP TABLE stored_games; DROP TABLES stored_moves;", 
            rusqlite::NO_PARAMS
        ).unwrap();
    }

    #[test]
    fn run_full_importer() {
        setup();

        let now = SystemTime::now();

        store_games_from_file("fixtures/pgn_import_tester.pgn".to_owned(), "Tezt".to_string(), &"bcnz".to_string(), true);

        match now.elapsed() {
            Ok(elapsed) => {
                println!("{} ms", elapsed.as_millis())
            }
            Err(e) => {
                println!("Error: {:?}", e);
            }
        }

        teardown();
    }
}
