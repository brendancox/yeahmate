use std::collections::HashMap;

#[derive(Clone, Copy, PartialEq)]
pub enum ChessPiece {
    NoPiece,
    WhitePawn,
    WhiteRook,
    WhiteKnight,
    WhiteBishop,
    WhiteQueen,
    WhiteKing,
    BlackPawn,
    BlackRook,
    BlackKnight,
    BlackBishop,
    BlackQueen,
    BlackKing
}

enum PieceComparison {
    EmptySquare,
    SameColour,
    OtherColour
}

enum Direction {
    Up,
    Down,
    Left,
    Right,
    UpLeft,
    DownLeft,
    UpRight,
    DownRight
}

#[derive(Clone)]
pub struct ChessMove {
    pub from: (usize, usize),
    pub to: (usize, usize),
    pub promote: Option<ChessPiece>,
    pub en_passant: bool,
    pub castle: bool
}

#[derive(Clone)]
pub struct PlayedMove {
    pub from: (usize, usize, ChessPiece),
    pub to: (usize, usize, ChessPiece),
    pub promote: Option<ChessPiece>,
    pub en_passant: bool,
    pub castle: bool,
    pub fullmove_number: usize,
    pub pre_no_move_fen: String,
    pub resulting_no_moves_fen: String,
    pub colour: ChessColour,
    pub check: bool,
    pub checkmate: bool,
    pub move_san: String // Does not include fullmove number or .. as these can be prepended when called.
}

impl PlayedMove {
    pub fn get_san(&self) -> String {
        let pgn = if self.colour == ChessColour::White {
            self.fullmove_number.to_string() + ". "
        } else {
            "".to_owned()
        };

        pgn + &self.move_san
    }
}

#[derive(Copy, Clone, PartialEq)]
pub enum ChessColour {
    White,
    Black
}

impl ChessMove {
    fn standard(from: (usize, usize), to: (usize, usize)) -> ChessMove {
        ChessMove {
            from: from,
            to: to,
            promote: None,
            en_passant: false,
            castle: false
        }
    }

    fn castle(from: (usize, usize), to: (usize, usize)) -> ChessMove {
        ChessMove {
            from: from,
            to: to,
            promote: None,
            en_passant: false,
            castle: true
        }
    }

    fn en_passant(from: (usize, usize), to: (usize, usize)) -> ChessMove {
        ChessMove {
            from: from,
            to: to,
            promote: None,
            en_passant: true,
            castle: false
        }
    }

    fn promotion(from: (usize, usize), to: (usize, usize), pawn: ChessPiece) -> Vec<ChessMove> {
        let promote_to = if pawn == ChessPiece::BlackPawn {
            vec![ChessPiece::BlackQueen, ChessPiece::BlackRook, ChessPiece::BlackKnight, ChessPiece::BlackBishop]
        } else {
            vec![ChessPiece::WhiteQueen, ChessPiece::WhiteRook, ChessPiece::WhiteKnight, ChessPiece::WhiteBishop]
        };

        let mut moves = vec![];
        
        for piece in promote_to {
            moves.push(ChessMove {
                from: from,
                to: to,
                promote: Some(piece),
                en_passant: false,
                castle: false
            });
        };

        moves
    }

    pub fn from_played_move(played_move: PlayedMove) -> Self {
        Self {
            from: (played_move.from.0, played_move.from.1),
            to: (played_move.to.0, played_move.to.1),
            promote: played_move.promote,
            en_passant: played_move.en_passant,
            castle: played_move.castle
        }
    }
}

impl Direction {
    fn next_square(&self, rank: usize, file: usize) -> Option<(usize, usize)> {
        let mut new_rank = rank;
        let mut new_file = file;

        match self {
            Direction::Up => {
                if new_rank == 0 {
                    return None;
                }
                new_rank -= 1
            },
            Direction::Down => {
                if new_rank == 7 {
                    return None
                }
                new_rank += 1
            },
            Direction::Left => {
                if new_file == 0 {
                    return None
                }
                new_file -= 1
            },
            Direction::Right => {
                if new_file == 7 {
                    return None
                }
                new_file += 1
            },
            Direction::UpLeft => {
                if new_rank == 0 || new_file == 0 {
                    return None;
                }
                new_rank -= 1;
                new_file -= 1;
            },
            Direction::DownLeft => {
                if new_rank == 7 || new_file == 0 {
                    return None;
                }
                new_rank += 1;
                new_file -= 1;
            },
            Direction::UpRight => {
                if new_rank == 0 || new_file == 7 {
                    return None;
                }
                new_rank -= 1;
                new_file += 1;
            },
            Direction::DownRight => {
                if new_rank == 7 || new_file == 7 {
                    return None;
                }
                new_rank += 1;
                new_file += 1; 
            }
        }

        Some((new_rank, new_file))
    }
}

impl ChessPiece {
    pub fn character(&self) -> &str {
        match self {
            ChessPiece::NoPiece => "",
            ChessPiece::WhitePawn => "P",
            ChessPiece::WhiteRook => "R",
            ChessPiece::WhiteKnight => "N",
            ChessPiece::WhiteBishop => "B",
            ChessPiece::WhiteQueen => "Q",
            ChessPiece::WhiteKing => "K",
            ChessPiece::BlackPawn => "p",
            ChessPiece::BlackRook => "r",
            ChessPiece::BlackKnight => "n",
            ChessPiece::BlackBishop => "b",
            ChessPiece::BlackQueen => "q",
            ChessPiece::BlackKing => "k"
        }
    }

    pub fn from_character(character: &char) -> ChessPiece {
        match character {
            'P' => ChessPiece::WhitePawn,
            'R' => ChessPiece::WhiteRook,
            'N' => ChessPiece::WhiteKnight,
            'B' => ChessPiece::WhiteBishop,
            'Q' => ChessPiece::WhiteQueen,
            'K' => ChessPiece::WhiteKing,
            'p' => ChessPiece::BlackPawn,
            'r' => ChessPiece::BlackRook,
            'n' => ChessPiece::BlackKnight,
            'b' => ChessPiece::BlackBishop,
            'q' => ChessPiece::BlackQueen,
            'k' => ChessPiece::BlackKing,
            _ => ChessPiece::NoPiece // Better would be exception. To do that later.
        }
    }

    fn directions(&self) -> Vec<Direction> {
        match self {
            ChessPiece::WhitePawn => vec![Direction::Up],
            ChessPiece::BlackPawn => vec![Direction::Down],
            ChessPiece::WhiteRook | ChessPiece::BlackRook => vec![
                Direction::Up,
                Direction::Down,
                Direction::Left,
                Direction::Right
            ],
            ChessPiece::WhiteBishop | ChessPiece::BlackBishop => vec![
                Direction::UpLeft,
                Direction::UpRight,
                Direction::DownLeft,
                Direction::DownRight
            ],
            ChessPiece::WhiteQueen | ChessPiece::WhiteKing | ChessPiece::BlackQueen | ChessPiece::BlackKing => vec![
                Direction::Up,
                Direction::Down,
                Direction::Left,
                Direction::Right,
                Direction::UpLeft,
                Direction::UpRight,
                Direction::DownLeft,
                Direction::DownRight
            ],
            _ => vec![]
        }
    }

    fn move_limit(&self) -> usize {
        match self {
            ChessPiece::WhitePawn | ChessPiece::BlackPawn | ChessPiece::WhiteKing | ChessPiece::BlackKing => 1,
            ChessPiece::WhiteRook | ChessPiece::BlackRook | ChessPiece::WhiteBishop | ChessPiece::BlackBishop | ChessPiece::WhiteQueen | ChessPiece::BlackQueen => 8,
            _ => 0
        }
    }

    fn is_white(&self) -> bool {
        match self {
            ChessPiece::WhitePawn | ChessPiece::WhiteRook | ChessPiece::WhiteKnight | ChessPiece::WhiteBishop | ChessPiece::WhiteQueen | ChessPiece::WhiteKing => true,
            _ => false
        }
    }

    fn is_colour(&self, colour: ChessColour) -> bool {
        if self == &ChessPiece::NoPiece {
            false
        } else if colour == ChessColour::White {
            self.is_white()
        } else {
            !self.is_white()
        }
    }

    fn can_move_to_square_with(&self, other: ChessPiece) -> PieceComparison {
        if other == ChessPiece::NoPiece {
            PieceComparison::EmptySquare
        } else {
            if self.is_white() == other.is_white() {
                PieceComparison::SameColour
            } else {
                PieceComparison::OtherColour
            }
        }
    }

    pub fn from_i64(int: i64) -> ChessPiece {
        let pieces = [
            ChessPiece::NoPiece,
            ChessPiece::WhitePawn,
            ChessPiece::WhiteRook,
            ChessPiece::WhiteKnight,
            ChessPiece::WhiteBishop,
            ChessPiece::WhiteQueen,
            ChessPiece::WhiteKing,
            ChessPiece::BlackPawn,
            ChessPiece::BlackRook,
            ChessPiece::BlackKnight,
            ChessPiece::BlackBishop,
            ChessPiece::BlackQueen,
            ChessPiece::BlackKing
        ];
        for x in pieces.iter() {
            if int == *x as i64 {
                return *x;
            }
        }

        ChessPiece::NoPiece
    }
}

impl ChessColour {
    pub fn from_i64(int: i64) -> ChessColour {
        if int == ChessColour::White as i64 {
            ChessColour::White
        } else {
            ChessColour::Black
        }
    }
}

pub fn square_notation_to_coords(square: &str) -> (usize, usize) {
    let file = square.chars().nth(0).unwrap();
    let rank = square.chars().nth(1).unwrap();

    let files = vec!['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
    let element = files.iter().position( |&character| character == file).unwrap();
    let row = 8 - rank.to_digit(10).unwrap();

    (row as usize, element)
}

pub fn coords_to_square_notation(coords: (usize, usize)) -> String {
    let file = vec!["a", "b", "c", "d", "e", "f", "g", "h"][coords.1].to_owned();
    file + &(8 - coords.0).to_string()
}

#[derive(Clone)]
pub struct GameInstance {
    pub placements: [[ChessPiece; 8]; 8],
    pub side_to_move: ChessColour,
    pub white_can_castle_kingside: bool,
    pub white_can_castle_queenside: bool,
    pub black_can_castle_kingside: bool,
    pub black_can_castle_queenside: bool,
    pub en_passant_target: Option<(usize, usize)>,
    pub halfmove_clock: usize,
    pub fullmoves: usize,
    pub played_moves: Vec<PlayedMove>
}


impl GameInstance {

    pub fn with_no_pieces() -> Self {
        Self {
            placements: [[ChessPiece::NoPiece; 8]; 8],
            side_to_move: ChessColour::White,
            white_can_castle_kingside: true,
            white_can_castle_queenside: true,
            black_can_castle_kingside: true,
            black_can_castle_queenside: true,
            en_passant_target: None,
            halfmove_clock: 0,
            fullmoves: 0,
            played_moves: vec![]
        }
    }

    pub fn starting_position() -> Self {
        let placement = [
            [ChessPiece::BlackRook, ChessPiece::BlackKnight, ChessPiece::BlackBishop, ChessPiece::BlackQueen, ChessPiece::BlackKing, ChessPiece::BlackBishop, ChessPiece::BlackKnight, ChessPiece::BlackRook],
            [ChessPiece::BlackPawn; 8],
            [ChessPiece::NoPiece; 8],
            [ChessPiece::NoPiece; 8],
            [ChessPiece::NoPiece; 8],
            [ChessPiece::NoPiece; 8],
            [ChessPiece::WhitePawn; 8],
            [ChessPiece::WhiteRook, ChessPiece::WhiteKnight, ChessPiece::WhiteBishop, ChessPiece::WhiteQueen, ChessPiece::WhiteKing, ChessPiece::WhiteBishop, ChessPiece::WhiteKnight, ChessPiece::WhiteRook]
        ];

        Self {
            placements: placement,
            side_to_move: ChessColour::White,
            white_can_castle_kingside: true,
            white_can_castle_queenside: true,
            black_can_castle_kingside: true,
            black_can_castle_queenside: true,
            en_passant_target: None,
            halfmove_clock: 0,
            fullmoves: 1,
            played_moves: vec![]
        }
    }

    pub fn from_fen(fen: &str) -> Self {
        let mut game = Self::with_no_pieces();
        game.set_from_fen(fen);
        game
    }

    pub fn set_from_fen(&mut self, fen: &str) {
        let mut parts = fen.split_whitespace();
        let placement_part = parts.next().unwrap();
        let mut rank = 0;
        let mut file = 0;
        for character in placement_part.chars() {
            if character == '/' {
                rank += 1;
                file = 0;
                continue;
            }
            if character.is_digit(10) {
                // Character is a base-10 digit (0-9).
                match character.to_digit(10) {
                    Some(advance) => {
                        self.placements[rank][file] = ChessPiece::NoPiece;
                        let final_file = file + (advance as usize);
                        while file < final_file {
                            let next_file = file + 1;
                            if next_file > 7 {
                                break;
                            } else {
                                file = next_file;
                                self.placements[rank][file] = ChessPiece::NoPiece;
                            }
                        }
                    },
                    None => ()
                };
                continue;
            }
            self.placements[rank][file] = ChessPiece::from_character(&character);
            if file < 7 {
                file += 1;
            }
        }

        let side_to_move_part = parts.next().unwrap();
        match side_to_move_part {
            "w" => {
                self.side_to_move = ChessColour::White;
            },
            "b" => {
                self.side_to_move = ChessColour::Black;
            },
            side_string => panic!("Bad fen (side to move): {}", side_string)
        }

        let castling_part = parts.next().unwrap();
        self.white_can_castle_kingside = castling_part.contains("K");
        self.white_can_castle_queenside = castling_part.contains("Q");
        self.black_can_castle_kingside = castling_part.contains("k");
        self.black_can_castle_queenside = castling_part.contains("q");

        let en_passant_part = parts.next().unwrap();
        self.en_passant_target = match en_passant_part {
            "-" => None,
            square => Some(square_notation_to_coords(square))
        };

        let halfmove_part = parts.next().unwrap();
        self.halfmove_clock = halfmove_part.parse().unwrap();

        let fullmove_part = parts.next().unwrap();
        self.fullmoves = fullmove_part.parse().unwrap();
    }

    pub fn get_fen(&self, no_moves: bool) -> String {
        let mut fen = "".to_owned();

        let mut empty_segment = 0;

        for (rank, row) in self.placements.iter().enumerate() {
            for piece in row.iter() {
                if piece == &ChessPiece::NoPiece {
                    empty_segment += 1;
                } else {
                    if empty_segment > 0 {
                        fen += &empty_segment.to_string();
                        empty_segment = 0;
                    }

                    fen += piece.character();
                }
            }

            if empty_segment > 0 {
                fen += &empty_segment.to_string();
                empty_segment = 0;
            }

            if rank < 7 {
                fen += "/";
            }
        }

        fen += " ";

        if self.side_to_move == ChessColour::White {
            fen += "w ";
        } else {
            fen += "b ";
        }

        if !self.white_can_castle_kingside && !self.white_can_castle_queenside
         && !self.black_can_castle_kingside && !self.black_can_castle_queenside {
            fen += "-";
        } else {
            if self.white_can_castle_kingside {
                fen += "K";
            }
            if self.white_can_castle_queenside {
                fen += "Q";
            }
            if self.black_can_castle_kingside {
                fen += "k";
            }
            if self.black_can_castle_queenside {
                fen += "q";
            }
        }

        fen += " ";

        match self.en_passant_target {
            None => fen += "-",
            Some(square) => {
                if no_moves {
                    // In the case of the opening db, we only specify this if there is supposed to
                    // be the possiblity of en passant happening.
                    if self.could_en_passant_happen() {
                        fen += &coords_to_square_notation(square);
                    } else {
                        fen += "-";
                    }
                } else {
                    // A full fen will specify the square even if moves are available or not.
                    fen += &coords_to_square_notation(square);
                }
            }
        }

        if no_moves {
            // We don't want the move counters. All done.
            return fen;
        }

        fen += " ";

        fen += &self.halfmove_clock.to_string();

        fen += " ";

        fen += &self.fullmoves.to_string();

        fen
    }

    fn could_en_passant_happen(&self) -> bool {
        // 5 2
        match self.en_passant_target {
            None => false,
            Some((rank, file)) => {
                if rank == 5 {
                    let upleft = match Direction::UpLeft.next_square(rank, file) {
                        None => false,
                        Some((next_rank, next_file)) => {
                            let piece = self.placements[next_rank][next_file];
                            piece == ChessPiece::BlackPawn
                        }
                    };
                    let upright = match Direction::UpRight.next_square(rank, file) {
                        None => false,
                        Some((next_rank, next_file)) => {
                            let piece = self.placements[next_rank][next_file];
                            piece == ChessPiece::BlackPawn
                        }
                    };

                    upleft || upright
                } else {
                    let downleft = match Direction::DownLeft.next_square(rank, file) {
                        None => false,
                        Some((next_rank, next_file)) => {
                            let piece = self.placements[next_rank][next_file];
                            piece == ChessPiece::WhitePawn
                        }
                    };
                    let downright = match Direction::DownRight.next_square(rank, file) {
                        None => false,
                        Some((next_rank, next_file)) => {
                            let piece = self.placements[next_rank][next_file];
                            piece == ChessPiece::WhitePawn
                        }
                    };

                    downleft || downright
                }
            }
        }
    }

    pub fn legal_moves_for_colour(&self, colour: ChessColour) -> Vec<ChessMove> {
        let mut moves = Vec::with_capacity(30);
        for (rank, row) in self.placements.iter().enumerate() {
            for (file, piece) in row.iter().enumerate() {
                if piece != &ChessPiece::NoPiece {
                    if colour == ChessColour::White {
                        if piece.is_white() {
                            moves.append(&mut self.legal_moves_from_square((rank, file)));
                        }
                    } else {
                        if !piece.is_white() {
                            moves.append(&mut self.legal_moves_from_square((rank, file)));
                        }
                    }
                }
            }
        }

        moves
    }

    pub fn maybe_get_legal_moves_from_cache(&self, colour: ChessColour, legal_moves_cache: Option<&mut HashMap<String, Vec<ChessMove>>>, halfmove: usize) -> Vec<ChessMove> {
        if halfmove > 30 || legal_moves_cache.is_none() {
            self.legal_moves_for_colour(colour)
        } else {
            let legal_moves_cache = legal_moves_cache.unwrap();
            let fen = self.get_fen(true);
            if !legal_moves_cache.contains_key(&fen) {
                let legal_moves = self.legal_moves_for_colour(colour);
                legal_moves_cache.insert(fen.clone(), legal_moves);
            }
            legal_moves_cache.get(&fen).unwrap().clone()
        }
    }

    fn is_position_check_for_colour(&self, colour: ChessColour) -> bool {

        // Find the king for 'colour'
        let target_king = if colour == ChessColour::White {
            ChessPiece::WhiteKing
        } else {
            ChessPiece::BlackKing
        };

        let mut target_square = (8, 8);
        for (rank, row) in self.placements.iter().enumerate() {
            for (file, piece) in row.iter().enumerate() {
                if piece == &target_king {
                    target_square = (rank, file);
                    break;
                }
            }
        }

        if target_square == (8, 8) {
            println!("Fen: {}", self.get_fen(false));
            for (rank, row) in self.placements.iter().enumerate() {
                for (file, piece) in row.iter().enumerate() {
                    println!("rank: {}, file: {}, piece: {}", rank, file, piece.character());
                }
            }
            panic!("No king!");
        }

        let other_colour = if colour == ChessColour::White {
            ChessColour::Black
        } else {
            ChessColour::White
        };

        // Iterate through each piece and check if it is a piece for the other colour.
        // If so, get all psuedo legal moves from that square.
        // Check if any have the destination that is the square the target king is on.
        // If so, return true.
        for (rank, row) in self.placements.iter().enumerate() {
            for (file, piece) in row.iter().enumerate() {
                if piece.is_colour(other_colour) {
                    let moves = self.pseudo_legal_moves_from_square(rank, file);
                    for chessmove in moves {
                        if chessmove.to == target_square {
                            return true;
                        }
                    }
                }
            }
        }

        false
    }

    pub fn legal_moves_from_square(&self, square: (usize, usize)) -> Vec<ChessMove> {
        let piece = self.placements[square.0][square.1];
        if piece == ChessPiece::NoPiece {
            return vec![];
        }

        let mut actual_legal = Vec::with_capacity(25);

        let pseudo_legal = self.pseudo_legal_moves_from_square(square.0, square.1);
        let colour = if piece.is_white() {
            ChessColour::White
        } else {
            ChessColour::Black
        };

        for chessmove in pseudo_legal {
            let mut cloned_game = self.clone();
            cloned_game.do_move(&chessmove, false, None);
            if cloned_game.is_position_check_for_colour(colour) {
                continue;
            }
            if chessmove.castle {
                if self.is_position_check_for_colour(colour) {
                    continue;
                }

                let in_between = match chessmove.to {
                    (7, 6) => (7, 5),
                    (7, 2) => (7, 3),
                    (0, 6) => (0, 5),
                    (0, 2) => (0, 3),
                    (_, _) => panic!("bad castle")
                };

                let mut castle_cloned_game = self.clone();
                castle_cloned_game.do_move(
                    &ChessMove::standard(chessmove.from, in_between),
                    false,
                    None
                );
                if castle_cloned_game.is_position_check_for_colour(colour) {
                    continue;
                }
            }
            actual_legal.push(chessmove);
        }

        actual_legal
    }

    fn pseudo_legal_moves_from_square(&self, rank: usize, file: usize) -> Vec<ChessMove> {
        let mut moves = Vec::with_capacity(25);
        let piece = self.placements[rank][file];

        let mut pawn_first_move = false;
        let mut pawn_to_promote = false;

        match piece {
            ChessPiece::BlackPawn | ChessPiece::WhitePawn => {

                // Pawns can each move 2 on their first move.
                if piece == ChessPiece::BlackPawn && rank == 1 {
                    pawn_first_move = true;
                }
                else if piece == ChessPiece::WhitePawn && rank == 6 {
                    pawn_first_move = true;
                }

                // If about to move to last rank, we need to add promotion moves.
                if piece == ChessPiece::BlackPawn && rank == 6 {
                    pawn_to_promote = true;
                }
                else if piece == ChessPiece::WhitePawn && rank == 1 {
                    pawn_to_promote = true;
                }

                // Pawns can take on a forward diagonal.
                let pawn_diagonals = if piece == ChessPiece::BlackPawn {
                    vec![Direction::DownLeft, Direction::DownRight]
                } else {
                    vec![Direction::UpLeft, Direction::UpRight]
                };

                for direction in pawn_diagonals {
                    match direction.next_square(rank, file) {
                        Some((next_rank, next_file)) => {
                            let other = self.placements[next_rank][next_file];
                            match piece.can_move_to_square_with(other) {
                                PieceComparison::OtherColour => {
                                    if pawn_to_promote {
                                        moves.append(&mut ChessMove::promotion((rank, file), (next_rank, next_file), piece));
                                    } else {
                                        moves.push(ChessMove::standard((rank, file), (next_rank, next_file)));
                                    }
                                },
                                PieceComparison::EmptySquare => {
                                    match self.en_passant_target {
                                        Some(en_passant_target_square) => {
                                            if en_passant_target_square == (next_rank, next_file) {
                                                moves.push(ChessMove::en_passant((rank, file), en_passant_target_square));
                                            }
                                        },
                                        None => ()
                                    }
                                },
                                _ => ()
                            }
                        },
                        None => ()
                    }
                }
            },
            ChessPiece::BlackKnight | ChessPiece::WhiteKnight  => {
                let knight_directions = vec![
                    (Direction::Up, Direction::UpLeft),
                    (Direction::Up, Direction::UpRight),
                    (Direction::Left, Direction::UpLeft),
                    (Direction::Right, Direction::UpRight),
                    (Direction::Down, Direction::DownLeft),
                    (Direction::Down, Direction::DownRight),
                    (Direction::Left, Direction::DownLeft),
                    (Direction::Right, Direction::DownRight),
                ];
                let mut knight_squares = vec![];
                for knight_direction in knight_directions {
                    match knight_direction.0.next_square(rank, file) {
                        Some((next_rank, next_file)) => {
                            match knight_direction.1.next_square(next_rank, next_file) {
                                Some((final_rank, final_file)) => {
                                    knight_squares.push((final_rank, final_file));
                                }, 
                                None => ()
                            }
                        },
                        None => ()
                    }
                }
                
                for knight_square in knight_squares {
                    let other = self.placements[knight_square.0][knight_square.1];
                    match piece.can_move_to_square_with(other) {
                        PieceComparison::SameColour => continue,
                        _ => moves.push(ChessMove::standard((rank, file), knight_square))
                    }
                }
            },
            // Castling
            ChessPiece::WhiteKing => {
                if self.white_can_castle_kingside {
                    let castle_squares = vec![(7, 5), (7, 6)];
                    let mut clear = true;
                    for castle_square in castle_squares {
                        if self.placements[castle_square.0][castle_square.1] != ChessPiece::NoPiece {
                            clear = false;
                            break;
                        }
                    }

                    if clear {
                        moves.push(ChessMove::castle((rank, file), (7, 6)));
                    }
                }
                if self.white_can_castle_queenside {
                    let castle_squares = vec![(7, 3), (7, 2), (7, 1)];
                    let mut clear = true;
                    for castle_square in castle_squares {
                        if self.placements[castle_square.0][castle_square.1] != ChessPiece::NoPiece {
                            clear = false;
                            break;
                        }
                    }

                    if clear {
                        moves.push(ChessMove::castle((rank, file), (7, 2)));
                    }
                }
            },
            ChessPiece::BlackKing => {
                if self.black_can_castle_kingside {
                    let castle_squares = vec![(0, 5), (0, 6)];
                    let mut clear = true;
                    for castle_square in castle_squares {
                        if self.placements[castle_square.0][castle_square.1] != ChessPiece::NoPiece {
                            clear = false;
                            break;
                        }
                    }

                    if clear {
                        moves.push(ChessMove::castle((rank, file), (0, 6)));
                    }
                }
                if self.black_can_castle_queenside {
                    let castle_squares = vec![(0, 3), (0, 2), (0, 1)];
                    let mut clear = true;
                    for castle_square in castle_squares {
                        if self.placements[castle_square.0][castle_square.1] != ChessPiece::NoPiece {
                            clear = false;
                            break;
                        }
                    }

                    if clear {
                        moves.push(ChessMove::castle((rank, file), (0, 2)));
                    }
                }
            },
            _ => ()
        }

        for direction in piece.directions() {
            let mut current_rank = rank;
            let mut current_file = file;
            let mut hit_opponent = false;
            let move_limit = if pawn_first_move {
                2
            } else {
                piece.move_limit()
            };
            for _ in 0..move_limit {
                match direction.next_square(current_rank, current_file) {
                    Some((next_rank, next_file)) => {
                        let other = self.placements[next_rank][next_file];
                        match piece.can_move_to_square_with(other) {
                            PieceComparison::EmptySquare => {
                                current_rank = next_rank;
                                current_file = next_file;
                            },
                            PieceComparison::SameColour => break,
                            PieceComparison::OtherColour => {
                                if piece == ChessPiece::BlackPawn || piece == ChessPiece::WhitePawn {
                                    break;
                                }
                                current_rank = next_rank;
                                current_file = next_file;
                                // So we know to break after adding the move.
                                hit_opponent = true;
                            }
                        }
                    },
                    None => break
                }

                if pawn_to_promote {
                    moves.append(&mut ChessMove::promotion((rank, file), (current_rank, current_file), piece));
                } else {
                    moves.push(ChessMove::standard((rank, file), (current_rank, current_file)));
                }

                if hit_opponent {
                    break;
                }
            }
        }

        moves
    }

    pub fn do_move(&mut self, chessmove: &ChessMove, full: bool, legal_moves_cache: Option<&mut HashMap<String, Vec<ChessMove>>>) {
        let piece = self.placements[chessmove.from.0][chessmove.from.1];
        let target = self.placements[chessmove.to.0][chessmove.to.1];
        let (alternative_moves, pre_fen) = if full {
            (
                self.maybe_get_legal_moves_from_cache(self.side_to_move, legal_moves_cache, self.played_moves.len()),
                self.get_fen(true)
            )
        } else {
            (vec![], "".to_string())
        };
        self.placements[chessmove.from.0][chessmove.from.1] = ChessPiece::NoPiece;
        self.placements[chessmove.to.0][chessmove.to.1] = match chessmove.promote {
            Some(newpiece) =>  newpiece,
            None => piece
        };

        if self.side_to_move == ChessColour::White {
            self.side_to_move = ChessColour::Black;
        } else {
            self.side_to_move = ChessColour::White;
        }

        // check castling settings.
        if piece == ChessPiece::WhiteKing {
            self.white_can_castle_queenside = false;
            self.white_can_castle_kingside = false;
        } else if piece == ChessPiece::BlackKing {
            self.black_can_castle_queenside = false;
            self.black_can_castle_kingside = false;
        }

        if piece == ChessPiece::WhiteRook {
            if chessmove.from == (7, 0) {
                self.white_can_castle_queenside = false;
            } else if chessmove.from == (7, 7) {
                self.white_can_castle_kingside = false;
            }
        } else if piece == ChessPiece::BlackRook {
            if chessmove.from == (0, 0) {
                self.white_can_castle_queenside = false;
            } else if chessmove.from == (0, 7) {
                self.white_can_castle_kingside = false;
            }
        }

        if chessmove.castle {
            if chessmove.to == (7,6) {
                self.placements[7][7] = ChessPiece::NoPiece;
                self.placements[7][5] = ChessPiece::WhiteRook;
            } else if chessmove.to == (7, 2) {
                self.placements[7][0] = ChessPiece::NoPiece;
                self.placements[7][3] = ChessPiece::WhiteRook;
            } else if chessmove.to == (0, 6) {
                self.placements[0][7] = ChessPiece::NoPiece;
                self.placements[0][5] = ChessPiece::BlackRook;
            } else if chessmove.to == (0, 2) {
                self.placements[0][0] = ChessPiece::NoPiece;
                self.placements[0][3] = ChessPiece::BlackRook;
            }
        }

        if piece == ChessPiece::WhitePawn {
            if chessmove.from.0 == 6 && chessmove.to.0 == 4 {
                self.en_passant_target = Some((5, chessmove.from.1));
            } else {
                self.en_passant_target = None;
            }
        } else if piece == ChessPiece::BlackPawn {
            if chessmove.from.0 == 1 && chessmove.to.0 == 3 {
                self.en_passant_target = Some((2, chessmove.from.1));
            } else {
                self.en_passant_target = None;
            }
        } else {
            self.en_passant_target = None;
        }

        if chessmove.en_passant {
            if chessmove.to.0 == 5 {
                self.placements[4][chessmove.to.1] = ChessPiece::NoPiece;
            } else if chessmove.to.0 == 2 {
                self.placements[3][chessmove.to.1] = ChessPiece::NoPiece;
            }
        }

        let colour = if piece.is_white() {
            ChessColour::White
        } else {
            ChessColour::Black
        };

        if self.side_to_move == ChessColour::White {
            self.fullmoves += 1;
        }

        if piece == ChessPiece::WhitePawn || piece == ChessPiece::BlackPawn {
            self.halfmove_clock = 0;
        } else if target != ChessPiece::NoPiece {
            // This means there was a capture.
            // Only other time there is a capture is en passant which is covered by pawn moves anyway.
            self.halfmove_clock = 0;
        } else {
            self.halfmove_clock += 1;
        }

        if full {
            let in_check = self.is_position_check_for_colour(self.side_to_move);
            let checkmate = in_check && self.legal_moves_for_colour(self.side_to_move).is_empty();

            let move_san = {
                if chessmove.castle {
                    if chessmove.to.1 == 6 {
                        "O-O".to_owned()
                    } else {
                        "O-O-O".to_owned()
                    }
                } else {
                    let mut san = coords_to_square_notation(chessmove.to);
                    let mut was_capture = false;
                    if target != ChessPiece::NoPiece || chessmove.en_passant {
                        was_capture = true;
                        san = "x".to_owned() + &san;
                    }
                    match chessmove.promote {
                        None => (),
                        Some(promoted_to) => {
                            san += "=";
                            san += &promoted_to.character().to_uppercase()
                        }
                    }

                    if checkmate {
                        san += "#";
                    } else if in_check {
                        san += "+";
                    }

                    // Disambiguation
                    let mut san_file = "".to_owned();
                    let mut san_rank = "".to_owned();
                    for alternative_move in alternative_moves {
                        if alternative_move.to == chessmove.to && alternative_move.from != chessmove.from {
                            let other_piece = self.placements[alternative_move.from.0][alternative_move.from.1];
                            if piece == other_piece {
                                if chessmove.from.0 != alternative_move.from.0 {
                                    san_file = vec!["a", "b", "c", "d", "e", "f", "g", "h"][chessmove.from.0].to_owned();
                                } else if chessmove.from.1 != alternative_move.from.1 {
                                    san_rank = (8 - chessmove.from.1).to_string();
                                }
                            }
                        }
                    }

                    san = san_rank + &san;
                    san = san_file.clone() + &san;

                    let piece_prepend = if piece == ChessPiece::WhitePawn || piece == ChessPiece::BlackPawn {
                        if san_file == "" && was_capture {
                            vec!["a", "b", "c", "d", "e", "f", "g", "h"][chessmove.from.0].to_owned()
                        } else {
                            "".to_owned()
                        }
                    } else {
                        piece.character().to_uppercase().to_owned()
                    };

                    piece_prepend + &san
                }
            };

            let fullmoves = if self.side_to_move == ChessColour::White {
                // We incremented before this to get an accurate fen
                // (although the full fen is no longer calculated, maybe it will be again later...)
                self.fullmoves - 1
            } else {
                self.fullmoves
            };

            self.played_moves.push(PlayedMove {
                from: (chessmove.from.0, chessmove.from.1, piece),
                to: (chessmove.to.0, chessmove.to.1, target),
                promote: chessmove.promote,
                en_passant: chessmove.en_passant,
                castle: chessmove.castle,
                fullmove_number: fullmoves,
                pre_no_move_fen: pre_fen,
                resulting_no_moves_fen: self.get_fen(true),
                colour: colour,
                check: in_check,
                checkmate: checkmate,
                move_san: move_san
            });
        }
    }

    pub fn create_move_from_san(&self, san: &str, legal_moves_cache: &mut HashMap<String, Vec<ChessMove>>, halfmove: usize) -> Result<ChessMove, String> {

        // checks don't matter to making the move. They're worked out afterwards.
        let processed_san = san.trim_end_matches(|c| c == '+' || c == '#');

        if processed_san == "O-O" {
            if self.side_to_move == ChessColour::White {
                return Ok(ChessMove::castle((7, 4), (7, 6)));
            } else {
                return Ok(ChessMove::castle((0, 4), (0, 6)));
            }
        }
        if processed_san == "O-O-O" {
            if self.side_to_move == ChessColour::White {
                return Ok(ChessMove::castle((7, 4), (7, 2)));
            } else {
                return Ok(ChessMove::castle((0, 4), (0, 2)));
            }
        }

        let promoted: Vec<&str> = processed_san.split("=").collect();
        let (mut processed_san, promote) = if promoted.len() == 2 {
            let promote_to = promoted[1];
            let promote_to = if self.side_to_move == ChessColour::White {
                promote_to.to_owned()
            } else {
                promote_to.to_lowercase()
            };
            (promoted[0].to_owned(), Some(ChessPiece::from_character(&promote_to.chars().next().unwrap())))
        } else {
            (processed_san.to_owned(), None)
        };

        let to_rank = processed_san.pop().unwrap();
        let to_file = processed_san.pop().unwrap();

        let mut piece = if self.side_to_move == ChessColour::White {
            ChessPiece::WhitePawn
        } else {
            ChessPiece::BlackPawn
        };

        let mut from_rank = None;
        let mut from_file = None;

        for (index, character) in processed_san.chars().enumerate() {
            if index > 2 {
                break;
            }

            if index == 0 {
                match character {
                    'B' => {
                        piece = if self.side_to_move == ChessColour::White {
                            ChessPiece::WhiteBishop
                        } else {
                            ChessPiece::BlackBishop
                        };
                        continue;
                    },
                    'N' => {
                        piece = if self.side_to_move == ChessColour::White {
                            ChessPiece::WhiteKnight
                        } else {
                            ChessPiece::BlackKnight
                        };
                        continue;
                    },
                    'R' => {
                        piece = if self.side_to_move == ChessColour::White {
                            ChessPiece::WhiteRook
                        } else {
                            ChessPiece::BlackRook
                        };
                        continue;
                    },
                    'K' => {
                        piece = if self.side_to_move == ChessColour::White {
                            ChessPiece::WhiteKing
                        } else {
                            ChessPiece::BlackKing
                        };
                        continue;
                    },
                    'Q' => {
                        piece = if self.side_to_move == ChessColour::White {
                            ChessPiece::WhiteQueen
                        } else {
                            ChessPiece::BlackQueen
                        };
                        continue;
                    },
                    _ => ()
                }
            }

            if character.is_alphabetic() {
                if character == 'x' {
                    break;
                }
                from_file = Some(character);
            } else {
                from_rank = Some(character);
            }
        }

        let files = vec!['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
        let ranks = vec!['8', '7', '6', '5', '4', '3', '2', '1'];

        let from_rank = match from_rank {
            Some(rank) => {
                match ranks.iter().position( |&character| character == rank) {
                    Some(rank) => Some(rank),
                    None => return Err(format!("Rank character did not correspond to anything. Character: {} from move {}", rank, san))
                }
            },
            None => None
        };

        let from_file = match from_file {
            Some(file) => {
                match files.iter().position( |&character| character == file) {
                    Some(file) => Some(file),
                    None => return Err(format!("File character did not correspond to anything. Character: {} from move {}", file, san))
                }
            },
            None => None
        };

        let to_rank = ranks.iter().position( |&character| character == to_rank).unwrap();
        let to_file = files.iter().position( |&character| character == to_file).unwrap();

        let legal_moves = self.maybe_get_legal_moves_from_cache(self.side_to_move, Some(legal_moves_cache), halfmove);

        for chessmove in legal_moves {
            if chessmove.to == (to_rank, to_file) {
                if promote.is_some() {
                    if chessmove.promote.is_none() {
                        continue;
                    }
                    if chessmove.promote.unwrap() == promote.unwrap() {
                        return Ok(chessmove);
                    }
                    continue;
                }

                let move_piece = self.placements[chessmove.from.0][chessmove.from.1];

                if move_piece != piece {
                    continue;
                }

                match from_file {
                    Some(ffile) => {
                        if ffile != chessmove.from.1 {
                            continue;
                        }
                    },
                    None => ()
                }

                match from_rank {
                    Some(frank) => {
                        if frank != chessmove.from.0 {
                            continue;
                        }
                    },
                    None => ()
                }

                // If we're still here, this is it.
                return Ok(chessmove);
            }
        }

        Err(format!("Move not found for {}", san))
    }


    pub fn create_move_from_uci(&self, uci: String) -> ChessMove {
        let legal_moves = self.legal_moves_for_colour(self.side_to_move);
        let mut from_uci = uci.clone();
        println!("uci to split {}", from_uci);
        let mut to_uci = from_uci.split_off(2);
        let promote = if to_uci.len() == 2 {
            None
        } else {
            println!("is promote? {}", to_uci);
            let character = to_uci.split_off(2);
            let character = if self.side_to_move == ChessColour::Black {
                character.to_lowercase()
            } else {
                character.to_uppercase()
            };

            Some(ChessPiece::from_character(&character.chars().next().unwrap()))
        };
        let from = square_notation_to_coords(from_uci.as_str());
        let to = square_notation_to_coords(to_uci.as_str());
        for chessmove in legal_moves {
            if chessmove.from == from && chessmove.to == to {
                match promote {
                    None => return chessmove,
                    Some(piece) => {
                        if chessmove.promote.is_none() {
                            continue;
                        }
                        let move_promote_piece = chessmove.promote.unwrap();
                        if move_promote_piece == piece {
                            return chessmove;
                        }
                    }
                }
            }
        }
        panic!("UCI move not found for {}", uci);
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use std::time::SystemTime;

    #[test]
    fn get_legal_moves() {
        let game = GameInstance::starting_position();

        let now = SystemTime::now();

        game.legal_moves_for_colour(game.side_to_move);

        println!("Calculated legal moves: {} \u{03bc}s", now.elapsed().unwrap().as_micros());
    }

    #[test]
    fn get_legal_moves_from_square() {
        let game = GameInstance::starting_position();

        let now = SystemTime::now();

        game.legal_moves_from_square((6, 0));

        println!("Calculated legal moves from square: {} \u{03bc}s", now.elapsed().unwrap().as_micros());
    }

    #[test]
    fn is_position_check_for_colour() {
        let game = GameInstance::starting_position();

        let now = SystemTime::now();

        game.is_position_check_for_colour(ChessColour::White);

        println!("Calculated is check: {} \u{03bc}s", now.elapsed().unwrap().as_micros());
    }

    #[test]
    fn pseudo_legal_from_sq() {
        let game = GameInstance::starting_position();

        let now = SystemTime::now();

        game.pseudo_legal_moves_from_square(6, 0);

        println!("pseudo legal moves: {} ns", now.elapsed().unwrap().as_nanos());
    }

    #[test]
    fn run_create_move_from_san() {

        let game = GameInstance::starting_position();

        let mut legal_moves_cache = HashMap::new();

        let now = SystemTime::now();
        let _chessmove = game.create_move_from_san("e4", &mut legal_moves_cache, 0).unwrap();
        println!("Create move from san: {} \u{03bc}s", now.elapsed().unwrap().as_micros());
    }


    #[test]
    fn run_get_fen_no_moves() {

        let game = GameInstance::starting_position();

        let now = SystemTime::now();
        let _chessmove = game.get_fen(false);
        println!("Fen_no_moves: {} \u{03bc}s", now.elapsed().unwrap().as_micros());
    }


    #[test]
    fn run_get_fen_with_move_nums() {

        let game = GameInstance::starting_position();

        let now = SystemTime::now();
        let _chessmove = game.get_fen(true);
        println!("Fen with moves: {} \u{03bc}s", now.elapsed().unwrap().as_micros());
    }

    #[test]
    fn do_a_move_full() {
        let mut game = GameInstance::starting_position();
        let mut legal_moves = game.legal_moves_for_colour(game.side_to_move);
        let move_to_do = legal_moves.pop().unwrap();

        let now = SystemTime::now();
        game.do_move(&move_to_do, true, None);
        println!("Do full move: {} \u{03bc}s", now.elapsed().unwrap().as_micros());
    }


    #[test]
    fn do_a_move_notfull() {
        let mut game = GameInstance::starting_position();
        let mut legal_moves = game.legal_moves_for_colour(game.side_to_move);
        let move_to_do = legal_moves.pop().unwrap();

        let now = SystemTime::now();
        game.do_move(&move_to_do, false, None);
        println!("Do notfull move: {} \u{03bc}s", now.elapsed().unwrap().as_micros());
    }
}
