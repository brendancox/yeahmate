use std::collections::HashMap;
use std::process::{Command, Stdio};
use std::io::{Write, BufReader, BufRead};

use super::chess_engine as chess_engine;
use chess_engine::{ChessColour, ChessMove, GameInstance};
use rusqlite::{Connection, params};
use stored_games::PlayerIs;
use super::openings as openings;
use super::stored_games as stored_games;

#[derive(Clone)]
pub struct AnalysedMove {
    pub chessmove: ChessMove,
    pub set_id: i64,
    pub white_wins: usize,
    pub black_wins: usize,
    pub draws: usize,
    pub no_result: usize
}

pub struct EngineMove {
    pub chessmove: ChessMove,
    pub centipawns: i64
}

pub struct AnalysedMoveSequence {
    pub moves: Vec<AnalysedMove>,
    pub pgn: String
}

pub struct EngineMoveSequence {
    pub moves: Vec<EngineMove>,
    pub pgn: String
}

pub fn next_book_moves(game: chess_engine::GameInstance) -> Vec<chess_engine::ChessMove> {
    let moves = game.legal_moves_for_colour(game.side_to_move);

    let mut book_moves = vec![];

    for chessmove in moves {
        if chessmove.to == (4, 3) {
            // d4
            book_moves.push(chessmove);
        } else if chessmove.to == (4, 4) {
            // e4
            book_moves.push(chessmove);
        }
    }

    book_moves
}

pub fn initialise_db() {
    let conn = Connection::open("gdchess.db").unwrap();
    openings::fill_opening_table(&conn);
    stored_games::create_tables(&conn);
    create_stats_tables(&conn);
}

fn create_stats_tables(conn: &Connection) {
    let statement = "CREATE TABLE IF NOT EXISTS filtered_set (
        id INTEGER PRIMARY KEY,
        set_id INTEGER,
        player_as INTEGER,
        since_date INTEGER,
        last_x_games INTEGER
    )";

    conn.execute(statement, params![]).unwrap();

    let statement = "CREATE TABLE IF NOT EXISTS stat_moves (
        id INTEGER PRIMARY KEY,
        previous INTEGER,
        filtered_set_id INTEGER,
        prior_pgn TEXT,
        halfmove INTEGER,
        move_san TEXT,
        white_wins INTEGER,
        black_wins INTEGER,
        draws INTEGER,
        no_result INTEGER,
        games INTEGER
    )";

    conn.execute(statement, params![]).unwrap();
}

pub fn store_games_from_file(path: String, set_name: String, player_name: String, quickstore: bool) {
    stored_games::store_games_from_file(path, set_name, &player_name, quickstore);
}

fn get_filtered_set_id(conn: &Connection, player_as: Option<stored_games::PlayerIs>, set_name: &str) -> i64 {
    let sql = "SELECT id FROM filtered_sets 
                    JOIN stored_game_sets ON filtered_sets.set_id = stored_game_sets.id
                    WHERE filtered_sets.player_as = ?
                    AND stored_game_sets.name = ?";
    let mut statement = conn.prepare(sql).unwrap();

    let player_as_param = match player_as {
        Some(player_is) => player_is as i64,
        None => PlayerIs::Neither as i64
    };

    let id = statement.query_row(params![player_as_param, set_name], |row| {
        Ok(row.get(0).unwrap())
    });

    match id {
        Ok(id) => return id,
        Err(_) => {
            let sql = "SELECT id from stored_game_sets WHERE name = ?";
            let mut statement = conn.prepare(sql).unwrap();
            let set_id: i64 = statement.query_row(params![set_name], |row| {
                Ok(row.get(0).unwrap())
            }).unwrap();
            let sql = "INSERT INTO filtered_sets (set_id, player_as) VALUES (?, ?)";
            let mut statement = conn.prepare(sql).unwrap();
            statement.execute(params![set_id, player_as_param]);
            conn.last_insert_rowid()
        }
    }
}

fn get_stat_moves(conn: &Connection, filtered_set_id: i64, pgn: &str, halfmove: usize, game: &chess_engine::GameInstance) -> Option<Vec<AnalysedMove>> {
    let mut statement = conn.prepare(
        "SELECT set_id
        FROM filtered_game_sets
        WHERE filtered_set_id = ?
    ").unwrap();
    let set_id: i64 = statement.query_row(params![filtered_set_id], |row| {
        Ok(row.get(0).unwrap())
    }).unwrap();
    
    let mut statement = conn.prepare(
        "SELECT move_san, white_wins, black_wins, draws, no_result
        FROM stat_moves
        WHERE filtered_set_id = ?
        AND halfmove = ?
        AND prior_pgn = ?
        ORDER BY games DESC
    ").unwrap();
    
    let stat_moves = statement.query_map(params![filtered_set_id, halfmove as i64, pgn], |row| 
        Ok((row.get::<_, String>(0).unwrap(), row.get::<_, i64>(1).unwrap() as usize, row.get::<_, i64>(2).unwrap() as usize, row.get::<_, i64>(3).unwrap() as usize, row.get::<_, i64>(4).unwrap()as usize))
    ).unwrap();

    let mut analysed_moves = Vec::new();
    let mut legal_moves_cache = HashMap::new();
    
    for stat_move in stat_moves {
        match stat_move {
            Ok((move_san, white_wins, black_wins, draws, no_result)) => {
                match game.create_move_from_san(move_san.as_str(), &mut legal_moves_cache, halfmove) {
                    Ok(chessmove) => {
                        analysed_moves.push(AnalysedMove {
                            chessmove,
                            set_id,
                            white_wins,
                            black_wins,
                            draws,
                            no_result
                        });
                    },
                    Err(_) => continue
                }
            },
            Err(_) => continue
        }
    }

    if analysed_moves.len() == 0 {
        // There will always be a placeholder entry if it's been analysed.
        None
    } else {
        Some(analysed_moves)
    }
}

fn calculate_stat_moves(conn: &Connection, game: &GameInstance, pgn: &str, set_name: &str, player_as: Option<stored_games::PlayerIs>, halfmove: usize) -> Vec<AnalysedMove> {
    let sql = "SELECT m.*, stored_game_sets.id, stored_games.result
                    FROM quickstore_moves m
                    JOIN stored_games ON m.game_id = stored_games.id
                    JOIN stored_game_sets ON stored_games.set_id = stored_game_sets.id
                    WHERE stored_game_sets.name = ?
                    AND m.halfmove = ?
                    AND stored_games.pgnmoves LIKE ?
    ".to_string();

    let sql = match player_as {
        Some(colour) => {
             let add = "AND stored_games.player_is = ".to_string() + (colour as i64).to_string().as_str();
             sql + add.as_str()
        },
        None => sql
                       
    };

    let mut statment = conn.prepare(&sql).unwrap();
    let played_moves = statment.query_map(params![set_name, halfmove as i64, pgn.to_string() + "%"], |row| {
               Ok((row.get(4).unwrap(), row.get::<_, String>(3).unwrap(), row.get::<_, i64>(2).unwrap() as usize, row.get(5).unwrap()))
            }).unwrap(); 

    let mut analysed_moves: HashMap<String, AnalysedMove> = HashMap::new();
    let mut legal_moves_cache = HashMap::new();

    for playedmove in played_moves {
        let (set_id, move_san, halfmove, result) = playedmove.unwrap();
        if !analysed_moves.contains_key(&move_san) {
            match game.create_move_from_san(move_san.as_str(), &mut legal_moves_cache, halfmove) {
                Ok(chessmove) => {
                    analysed_moves.insert(move_san.clone(), AnalysedMove {
                        chessmove,
                        set_id,
                        white_wins: 0,
                        black_wins: 0,
                        draws: 0,
                        no_result: 0
                    });
                },
                Err(_) => continue
            }
        }

        let analysed_move  = analysed_moves.get_mut(&move_san).unwrap();
        match stored_games::GameResult::from_i64(result) {
            stored_games::GameResult::NoResult => analysed_move.no_result += 1,
            stored_games::GameResult::Draw => analysed_move.draws += 1,
            stored_games::GameResult::WhiteWins => analysed_move.white_wins += 1,
            stored_games::GameResult::BlackWins => analysed_move.black_wins += 1
        }
    }

    let mut analysed_moves: Vec<AnalysedMove> = analysed_moves.values().cloned().collect();

    analysed_moves.sort_by(|a, b| {
        let a_total = a.white_wins + a.black_wins + a.draws;
        let b_total = b.white_wins + b.black_wins + b.draws;
        a_total.partial_cmp(&b_total).unwrap().reverse()
    });

    analysed_moves
}

fn save_stat_moves(conn: &Connection, moves: Vec<AnalysedMove>) {

}

pub fn order_quickmoves_according_to_set(game: GameInstance, pgn: String, set_name: String, player_as: Option<stored_games::PlayerIs>, halfmoves_ahead: i64) -> Vec<AnalysedMove> {
    let mut halfmove = (game.fullmoves - 1) * 2;
    if game.side_to_move == ChessColour::Black {
        halfmove += 1;
    }
    let conn = Connection::open("gdchess.db").unwrap();


    let filtered_set_id = get_filtered_set_id(&conn, player_as, set_name.as_str());

    match get_stat_moves(&conn, filtered_set_id, pgn.as_str(), halfmove, &game) {
        Some(moves) => return moves,
        None => {
            let moves = calculate_stat_moves(&conn, &game, pgn.as_str(), set_name.as_str(), player_as, halfmove);
            save_stat_moves(&conn, moves.clone());
            return moves
        }
    }
}

pub fn get_set_names() -> Vec<String> {
    let conn = Connection::open("gdchess.db").unwrap();
    let mut statement = conn.prepare("SELECT * FROM stored_game_sets").unwrap();
    let sets = statement.query_map(params![], |row| {
        Ok(row.get(1).unwrap())
    }).unwrap();

    let mut setnames = Vec::new();
    for set in sets {
        setnames.push(set.unwrap());
    }

    setnames
}

pub fn next_engine_move(fen: String) -> Vec<EngineMove> {
    let mut game = GameInstance::from_fen(fen.as_str());
    let mut stockfish = Command::new("/home/brendan/projects/chess/Stockfish/src/stockfish")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn().unwrap();
    println!("Stockfish spawned");

    let stdout = stockfish.stdout.as_mut().unwrap();
    let mut reader = BufReader::new(stdout);
    {
        let stdin = stockfish.stdin.as_mut().unwrap();
        stdin.write_all(b"uci\n").unwrap();
    }
    let mut buffer = String::new();
    loop {
        match reader.read_line(&mut buffer) {
            Err(_) => break,
            Ok(length) => {
                if length == 0 {
                    break;
                }
                print!("{}", &mut buffer);

                if buffer.contains("uci") {
                    break;
                }

                buffer.clear();
            }
        }
    }
    buffer.clear();

    {
        let stdin = stockfish.stdin.as_mut().unwrap();
        stdin.write_all(b"setoption name MultiPV value 5\nisready\n").unwrap();
    }

    reader.read_line(&mut buffer).unwrap();
    print!("{}", buffer);
    buffer.clear();

    {
        let stdin = stockfish.stdin.as_mut().unwrap();
        let instructions = format!("ucinewgame\nposition fen {}\ngo depth 10\n", fen);
        stdin.write_all(instructions.as_bytes()).unwrap();
    }

    let mut analysed_moves = Vec::new();

    loop {
        match reader.read_line(&mut buffer) {
            Err(_) => break,
            Ok(length) => {
                if length == 0 {
                    break;
                }
                print!("{}", &mut buffer);
                
                if buffer.contains("info depth 10") {
                    let mut score = 0;
                    let mut chosen_move = "";
                    let mut next_is_score = false;
                    let mut next_is_move = false;
                    for token in buffer.split_whitespace() {
                        if token == "cp" {
                            next_is_score = true;
                            continue;
                        }
                        if token == "pv" {
                            next_is_move = true;
                            continue;
                        }
                        if next_is_score {
                            score = token.parse::<i64>().unwrap();
                            next_is_score = false;
                        }
                        if next_is_move {
                            chosen_move = token;
                            break;
                        }
                    }

                    analysed_moves.push(EngineMove {
                        chessmove: game.create_move_from_uci(chosen_move.to_string()),
                        centipawns: score
                    });
                }

                if buffer.contains("bestmove") {
                    break;
                }

                buffer.clear();
            }
        }
    }
    buffer.clear();

    stockfish.wait().unwrap();

    analysed_moves
}
