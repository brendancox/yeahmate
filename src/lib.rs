use chess_engine::GameInstance;
use gdnative::prelude::*;

mod chess_engine;
mod analysis;
mod openings;
mod stored_games;


#[derive(NativeClass)]
#[inherit(Node)]
struct ChessGame {
    current: chess_engine::GameInstance
}

#[gdnative::methods]
impl ChessGame {
    fn new(_owner: &Node) -> Self {
        ChessGame {
            current: chess_engine::GameInstance::with_no_pieces()
        }
    }

    #[export]
    fn _ready(&self, _owner: &Node) {

    }
    
    #[export]
    fn get_placement_matrix(&self, _owner: &Node) -> VariantArray {
        let g_placement = VariantArray::new();
        for row in self.current.placements.iter() {
            let mut g_row = StringArray::new();
            for piece in row.iter() {
                g_row.push(piece.character().into());
            }
            g_placement.push(g_row);
        }
        g_placement.into_shared()
    }

    #[export]
    fn set_fen(&mut self, _owner: &Node, fen: GodotString) {
        self.current.set_from_fen(fen.to_utf8().as_str());
    }

    #[export]
    fn get_fen(&self, _owner: &Node) -> GodotString {
        GodotString::from_str(self.current.get_fen(false))
    }

    #[export]
    fn legal_moves(&self, _owner: &Node) -> VariantArray {
        let moves = self.current.legal_moves_for_colour(self.current.side_to_move);

        let g_moves = VariantArray::new();

        for chessmove in moves {
            g_moves.push(chessmove_to_godot(&chessmove));
        }

        g_moves.into_shared()
    }

    #[export]
    fn legal_moves_from_square(&self, _owner: &Node, g_square: GodotString) -> VariantArray {
        let square = g_square.to_utf8();
        let (rank, file) = chess_engine::square_notation_to_coords(square.as_str());
        let chessmoves = self.current.legal_moves_from_square((rank, file));

        let g_moves = VariantArray::new();

        for chessmove in chessmoves {
            g_moves.push(chessmove_to_godot(&chessmove));
        }

        g_moves.into_shared()
    }

    #[export]
    fn do_move_from_godot(&mut self, _owner: &Node, g_move: Dictionary) {
        self.current.do_move(&chessmove_from_godot(g_move), true, None);
    }

    #[export]
    fn side_to_move(&self, _owner: &Node) -> GodotString {
        if self.current.side_to_move == chess_engine::ChessColour::White {
            GodotString::from_str("w")
        } else {
            GodotString::from_str("b")
        }
    }

    #[export]
    fn get_opening(&self, _owner: &Node) -> GodotString {
        let opening = openings::get_opening_by_game(&self.current);

        match opening {
            None => GodotString::from_str(""),
            Some(opening) => GodotString::from_str(&opening)
        }
    }

    #[export]
    fn get_pgn(&self, _owner: &Node) -> GodotString {
        let mut pgn = "".to_owned();

        for played_move in self.current.played_moves.iter() {
            pgn += &played_move.get_san();
            pgn += " ";
        }

        GodotString::from_str(&pgn)
    }
}

fn chessmove_from_godot(g_move: Dictionary) -> chess_engine::ChessMove {
    let from = g_move.get(Variant::from_str("from")).to_int32_array();
    let from = from.read();
    let to = g_move.get(Variant::from_str("to")).to_int32_array();
    let to = to.read();
    let g_promote = g_move.get(Variant::from_str("promote")).to_godot_string().to_utf8();
    let promote = match g_promote.as_str() {
        "" => None,
        piece_str => Some(chess_engine::ChessPiece::from_character(&piece_str.chars().nth(0).unwrap()))
    };
    let en_passant = g_move.get(Variant::from_str("en_passant")).to_bool();
    let castle = g_move.get(Variant::from_str("castle")).to_bool();

    chess_engine::ChessMove {
        from: (from.as_slice()[0] as usize, from.as_slice()[1] as usize),
        to: (to.as_slice()[0] as usize, to.as_slice()[1] as usize),
        promote: promote,
        en_passant: en_passant,
        castle: castle
    }
}

fn chessmove_to_godot(chessmove: &chess_engine::ChessMove) -> Dictionary {
    let from = Variant::from_str("from");
    let to = Variant::from_str("to");
    let promote = Variant::from_str("promote");
    let en_passant = Variant::from_str("en_passant");
    let castle = Variant::from_str("castle");

    let g_move = Dictionary::new();
    g_move.insert(&from, Int32Array::from_vec(vec![chessmove.from.0 as i32, chessmove.from.1 as i32]));
    g_move.insert(&to, Int32Array::from_vec(vec![chessmove.to.0 as i32, chessmove.to.1 as i32]));

    match chessmove.promote {
        Some(piece) => g_move.insert(&promote, Variant::from_str(piece.character())),
        None => g_move.insert(&promote, Variant::from_str("")),
    }

    g_move.insert(&en_passant, Variant::from_bool(chessmove.en_passant));
    g_move.insert(&castle, Variant::from_bool(chessmove.castle));

    g_move.into_shared()
}

#[derive(NativeClass)]
#[inherit(Node)]
struct Analyser {
}

#[gdnative::methods]
impl Analyser {
    fn new(_owner: &Node) -> Self {

        Analyser {
        }
    }

    #[export]
    fn _ready(&self, _owner: &Node) {
        analysis::initialise_db();
    }

    #[export]
    fn next_book_moves(&self, _owner: &Node, fen: GodotString) -> VariantArray {
        let mut game = chess_engine::GameInstance::with_no_pieces();
        game.set_from_fen(fen.to_utf8().as_str());
        
        let g_moves = VariantArray::new();

        for chessmove in analysis::next_book_moves(game) {
            g_moves.push(chessmove_to_godot(&chessmove));
        }

        g_moves.into_shared()
    }

    #[export]
    fn get_opening_by_fen(&self, _owner: &Node, fen: GodotString) -> GodotString {
        let mut game = chess_engine::GameInstance::with_no_pieces();
        game.set_from_fen(fen.to_utf8().as_str());

        let opening = openings::get_opening_by_fen(game.get_fen(true));

        match opening {
            None => GodotString::from_str(""),
            Some(opening) => GodotString::from_str(&opening)
        }
    }

    #[export]
    fn store_games_from_file(&self, _owner: &Node, path: GodotString, set_name: GodotString, player_name: GodotString, quickstore: Variant) {
        analysis::store_games_from_file(
            path.to_utf8().as_str().to_owned(),
            set_name.to_utf8().as_str().to_owned(),
            player_name.to_utf8().as_str().to_owned(),
            quickstore.to_bool()
        );
    }


    #[export]
    fn order_moves_with_pgn_set(&self, _owner: &Node, fen: GodotString, pgn: GodotString, set_name: GodotString, player_as: GodotString, halfmoves_ahead: Variant) -> VariantArray {
        let game = GameInstance::from_fen(fen.to_string().as_str());

        let player_as = match player_as.to_string().as_str() {
            "white" => Some(stored_games::PlayerIs::White),
            "black" => Some(stored_games::PlayerIs::Black),
            _ => None
        };

        let halfmoves = halfmoves_ahead.to_i64();

        let analysed_moves = analysis::order_quickmoves_according_to_set(game, pgn.to_string(), set_name.to_string(), player_as, halfmoves);

        let g_moves = VariantArray::new();
        for analysed_move in analysed_moves {
            let g_move = Dictionary::new();
            g_move.insert(Variant::from_str("chessmove"), chessmove_to_godot(&analysed_move.chessmove));
            g_move.insert(Variant::from_str("count"), Variant::from_u64((analysed_move.white_wins + analysed_move.black_wins + analysed_move.draws) as u64));
            g_moves.push(g_move);
        }
        g_moves.into_shared()
    }

    #[export]
    fn get_set_names(&self, _owner: &Node) -> StringArray {
        let setnames = analysis::get_set_names();
        let mut godotsets = StringArray::new();
        for set in setnames {
            godotsets.push(GodotString::from(set));
        }

        godotsets
    }

    #[export]
    fn next_engine_moves(&self, _owner: &Node, fen: GodotString) -> VariantArray {
        let analysed_moves = analysis::next_engine_move(fen.to_string());
        
        let g_moves = VariantArray::new();
        for analysed_move in analysed_moves {
            let g_move = Dictionary::new();
            g_move.insert(Variant::from_str("chessmove"), chessmove_to_godot(&analysed_move.chessmove));
            g_move.insert(Variant::from_str("score"), Variant::from_i64(analysed_move.centipawns));
            g_moves.push(g_move);
        }
        g_moves.into_shared()
    }
}

fn init(handle: InitHandle) {
    handle.add_class::<ChessGame>();
    handle.add_class::<Analyser>();
}

godot_init!(init);
