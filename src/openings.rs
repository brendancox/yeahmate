use rusqlite::{params, Connection, NO_PARAMS, OptionalExtension, Result};
use std::fs::File;
use std::io::{BufReader, BufRead};
use super::chess_engine as chess_engine;

#[derive(Debug)]
struct Opening {
    id: usize,
    eco: String,
    name: String,
    fen: String,
    moves: String
}

pub fn fill_opening_table(conn: &Connection) {
    let statement = "CREATE TABLE IF NOT EXISTS openings (
        id INTEGER PRIMARY KEY,
        eco TEXT NOT NULL,
        name TEXT NOT NULL,
        fen TEXT NOT NULL,
        moves TEXT NOT NULL
    )";

    conn.execute(statement, params![]).unwrap();

    let opening_count: u32 = conn.query_row("SELECT COUNT(*) FROM openings", NO_PARAMS, |row| row.get(0)).unwrap();

    if opening_count == 0 {
        for tsv in vec!["a.tsv", "b.tsv", "c.tsv", "d.tsv", "e.tsv"] {
            match File::open("external/".to_owned() + tsv) {
                Ok(file) => {
                    for line in BufReader::new(file).lines() {
                        match line {
                            Ok(opening_str) => {
                                let fields: Vec<&str> = opening_str.split("\t").collect();
                                if fields[0] != "eco" {
                                    // The top line will be the headings, so we've excluded that.
                                    conn.execute("INSERT INTO openings (eco, name, fen, moves) VALUES (?, ?, ?, ?)", fields).unwrap();
                                }
                            },
                            _ => ()
                        }
                    }
                },
                _ => ()
            }
        }
    }
}

pub fn get_opening_by_fen(fen: String) -> Option<String> {
    let conn = Connection::open("gdchess.db").unwrap();
    let opening: Result<Option<String>> = conn.query_row("SELECT name FROM openings WHERE fen = ?", params![fen], |row| row.get(0)).optional();
    opening.unwrap()
}

pub fn get_opening_by_game(game: &chess_engine::GameInstance) -> Option<String> {
    let conn = Connection::open("gdchess.db").unwrap();
    for played_move in game.played_moves.iter().rev() {
        let opening: Result<Option<String>> = conn.query_row("SELECT name FROM openings WHERE fen = ?", params![played_move.resulting_no_moves_fen], |row| row.get(0)).optional();
        let opening_option = opening.unwrap();
        if opening_option.is_some() {
            return opening_option;
        }
    }

    None
}
